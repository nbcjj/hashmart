# HashMart (哈希玛特)开源盲盒系统

<div align="center" >
    <a href="https://hashmart.cc">
        <img src="https://img.shields.io/badge/Licence-LGPL2.1-green.svg?style=flat" />
    </a>
    <a href="https://hashmart.cc">
        <img src="https://img.shields.io/badge/Edition-4.5-blue.svg" />
    </a>
     <a href="https://gitee.com/kitego/hashmart/repository/archive/master.zip">
        <img src="https://img.shields.io/badge/Download-40m-red.svg" />
    </a>
</div>

## 介绍
HashMart（哈希玛特）是首款开源的面向生产的高性能、易开发的盲盒系统。它包含：首页看板、商品管理、订单管理、盲盒管理、系统管理、会员管理、权限管理、记录管理，并且拥有完善的搭建使用手册和接口文档。是帮助您快速落地盲盒商城系统的不二之选。

## 软件架构
ThinkPHP6.0 + VUE3.0 + UNIAPP


## 安装教程

手册地址：http://doc.hashmart.cc

## 使用说明

手册地址：http://doc.hashmart.cc

## 系统演示

https://hashmart.cc/preview.html

## 交流群
![输入图片说明](screenshoot/qrcode.jpg)

 **专属官方群，进入步骤** ：  
1、进入官网，点击开源版下载，进入开源代码仓库  
2、点击star，成功截图发给客服  
3、客服标记，后续拉入专属官方群  

## 页面预览

### 首页
![输入图片说明](screenshoot/home.png)

### 商品
![输入图片说明](screenshoot/goods.png)

### 订单
![输入图片说明](screenshoot/order.png)

### 盲盒
![输入图片说明](screenshoot/blindbox.png)

### 系统
![输入图片说明](screenshoot/system.png)

### 会员
![输入图片说明](screenshoot/user.png)

### 权限
![输入图片说明](screenshoot/rule.png)

### 记录
![输入图片说明](screenshoot/log.png)

### 前端介绍
![输入图片说明](screenshoot/introduct.jpg)

## 特别鸣谢
排名不分先后，感谢这些软件的开发者：thinkphp、vue、mysql、redis、uniapp、echarts、scui、elementui-plus 等，如有遗漏请联系我！

## 版权说明

1.允许用于个人学习、毕业设计、教学案例、公益事业、商业使用;  
2.如果商用必须保留版权信息，请自觉遵守;  
3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。  

## 去版权商用说明

哈希玛特开源版-购买去版权商用证书：  
使用说明;  
1.去版权授权版可以去除代码中包含版权的信息;  
2.需要提供商用站点的域名+IP，我方提供商用授权证书；仅可在授权的站点下去版权商用;  
包含服务：  
3.享受首年免费技术咨询服务;  
4.享受包搭建系统一次;  
5.享受开源版永久免费升级;  
价格说明：  
6.开源版去版权商用证书，4800元，永久授权使用；仅支持当前授权的域名+IP;

## 软件著作权证书

![输入图片说明](%E5%93%88%E5%B8%8C%E7%8E%9B%E7%89%B9.jpg)

